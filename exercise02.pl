use v5.26;

my %worlds = (
        Mercury  => "Fleet in the heat",
        Venus    => "Beauty veiled in mystery",
        Earth    => "Cradle of life",
        Mars     => "Bringer of war",
        Jupiter  => "Lord of the system",
        Saturn   => "Ringéd wonder",
        Neptune  => "The not-particularly interesting",
        Uranus   => "Bringer of rude puns",
        Pluto    => "...is no longer a planet!",
);

my @que_worlds;

while (my $input = readline()) {
    chomp($input);
    my @req_words = split " ", $input;

    for my $world (@req_words) {
        if (!grep($world, @que_worlds)) {
            push @que_worlds, ucfirst(lc($world));
        }
    }

    say $worlds{shift @que_worlds};
}