use v5.26;
use List::Util qw<min max>;
use Statistics::Basic qw<mean mode median>;

# min, max, average, media and mode

my $file_name = 'numbers.txt';
my @numbers;

open my $fh, '<', $file_name
  or die "Can't open file : $!";

while(my $info = <$fh>){
  # Delete newline
  chomp($info);

  push @numbers, $info;
}

say min(@numbers) . "\n";
say max(@numbers) . "\n";
say mean(@numbers) . "\n";
say median(@numbers) . "\n";
say mode(@numbers) . "\n";