use v5.26;

use lib '/Users/Legolas/Desktop/School/2017HS/perl/code/assignment/lib';
use Exam::Report;

# given question block separator
my $QUESTION_SEPARATOR  = "________________________________________________________________________________";

#read given file name for path
my ($master_file_name, @student_file_names) = @ARGV;

my $master_question_set = get_question_set($master_file_name);

my $reports_ref = score($master_question_set, @student_file_names);

report_results($reports_ref);

sub get_question_set {
  my ($file_name) = @_;
  my %question_set;

  # open files
  open my $file_handler, '<', $file_name
    or die "Can't open file : $!";

  # log processed questions
  my $questions_processed = 0;

  {
    # split the file by the question separator
    local $/ = $QUESTION_SEPARATOR;
    while(my $question_block = <$file_handler>){
      # skip the header
      if ($questions_processed) {
        my $question = get_question($question_block);
        my @answers = @{ get_answers($question_block) };

        $question_set{$question} = \@answers;
      }

      $questions_processed++;
    }
  }

  # close the files
  close $file_handler or die "Couldn't Close File : $!";

  return \%question_set;
}

sub score {
  my ($mqs_ref, @student_file_names) = @_;
  my %master_question_set = %{ $mqs_ref };
  my @reports;

  foreach my $student_file_name (@student_file_names) {
    my $report = new Exam::Report ($student_file_name);
    my %question_set = %{ get_question_set($student_file_name) };
    my $score = 0;

    foreach my $m_question (keys %master_question_set) {
      if($question_set{$m_question}) {
        $score += eval_answers($report, $master_question_set{$m_question}, $question_set{$m_question});
      } else {
        $report->add_missing_question($m_question);
      }
    }

    $report->set_score($score);
    $report->set_no_questions(scalar keys %question_set);
    push @reports, $report;
  }

  return \@reports;
}

sub report_results {
  my ($reports_ref) = @_;

  score_report($reports_ref);

  say '';
  missing_questions_report($reports_ref);

  say '';
  missing_answers_report($reports_ref);
}

sub score_report {
  my ($reports_ref) = @_;

  foreach my $report (@{ $reports_ref }) {
    my $exam = $report->get_exam();
    my $noq = $report->get_no_questions();
    my $score = $report->get_score();

    printf($exam . "\t\t%02d/%02d\n",$score,$noq);
  }
}

sub missing_questions_report {
  my ($reports_ref) = @_;

  foreach my $report (@{ $reports_ref }) {
    my $exam = $report->get_exam();
    my @m_q = @{ $report->get_missing_questions() };
    if ($m_q[0]) {
      say $exam;
      foreach my $q (@m_q) {
        say "\t".$q;
      }
    }
  }
}

sub missing_answers_report {
  my ($reports_ref) = @_;

  foreach my $report (@{ $reports_ref }) {
    my $exam = $report->get_exam();
    my @m_a = @{ $report->get_missing_answers() };
    if ($m_a[0]) {
      say $exam;
      foreach my $a (@m_a) {
        say "\t".$a;
      }
    }
  }
}

sub get_answers {
  my ($question_block) = @_;

  my @answers = $question_block =~ m/\N*(\[\s*(?:\w|\s)\s*\].*)/gxm;

  return \@answers;
}

sub clean_answer {
  my ($answer) = @_;
  $answer =~ s/^\s*\[[^]]*\w?[^]]*\]//m;

  return $answer;
}

sub clean_answers {
  my ($a_ref) = @_;
  my @answers = @{ $a_ref };

  foreach my $answer (@answers) {
    $answer = clean_answer($answer);
  }

  return \@answers;
}

sub get_question {
  my ($question) = @_;

  #                     1
  $question =~ m/\d+\.\s(.+?[\?:\.])\n/gmxs; #.+? is getting shit lazy

  return clean_question($1);
}

sub clean_question {
  my ($question) = @_;

  $question =~ s/\s{2,}//;

  return $question;
}

sub eval_answers {
  my ($report, $ma_ref, $sa_ref) = @_;
  my @master_answers = @{ $ma_ref };
  my @student_answers = @{ $sa_ref };
  report_missing_answers($report, $ma_ref, $sa_ref);

  my $correct_answer = get_ticked_answer(@master_answers);
  my $ticked_answer = get_ticked_answer(@student_answers);

  if($ticked_answer >= 0) {
    if(clean_answer($master_answers[$correct_answer]) eq clean_answer($student_answers[$ticked_answer])) {
      return 1; # only if but one answer was ticked and it equals the correct on the mark is given
    } else {
      return 0;
    }
  } else {
    return 0; # no or tomany answers given
  }
}

sub report_missing_answers {
  my ($report, $ma_ref, $sa_ref) = @_;
  my @master_answers = @{ clean_answers($ma_ref) };
  my @student_answers = @{ clean_answers($sa_ref) };

  foreach my $answer (@master_answers) {

    if (!grep {$answer eq $_} @student_answers) { # report all questions that are not in students_answers
      $report->add_missing_answer($answer);
    }
  }
}

sub get_ticked_answer {
  my (@answers) = @_;
  my $index = -1; # if nothing has been ticked it is -1

  for (my $i = 0; $i <= $#answers; $i++) {
    if($answers[$i] =~ m/^\s*\[[^]]*\w[^]]*\]/) {
      if ($index == -1) {
        $index = $i;
      } else {
        return -1; # if at least two have been ticked the answer set is invalid already
      }
    } 
  }

  return $index; # return the index of the ticked question or -1 if none have been ticked.
}