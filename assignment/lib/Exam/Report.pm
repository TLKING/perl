package Exam::Report;
our $VERSION = '0.000001';

use v5.26;

sub new {
  my $class = shift;
  $class = ref $class if ref $class;

  my $self = bless {
      exam => shift,
      no_questions => 0,
      score => 0,
      missing_questions => [],
      missing_answers => []
    }, $class;
  return $self;
}

sub get_exam {
  my ($self) = @_;
  return $self->{exam};
}

sub get_score {
  my ($self) = @_;
  return $self->{score};
}

sub get_no_questions {
  my ($self) = @_;
  return $self->{no_questions};
}

sub get_missing_questions {
  my ($self) = @_;
  return $self->{missing_questions};
}

sub get_missing_answers {
  my ($self) = @_;
  return $self->{missing_answers};
}

sub add_missing_question {
  my ($self, $question) = @_;

  push @{ ($self->{missing_questions}) }, $question;
}

sub add_missing_answer {
  my ($self, $answer) = @_;

  push @{ $self->{missing_answers} }, $answer;
}

sub set_no_questions {
  my ($self, $noq) = @_;

  $self->{no_questions} = $noq if defined $noq;
}

sub set_score {
  my ($self, $score) = @_;

  $self->{score} = $score if defined $score;
}

"Magic true value, cause reasons.";