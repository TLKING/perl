use v5.26;
use File::Spec;
# given question block separator
my $QUESTION_SEPARATOR  = "________________________________________________________________________________";

#read given file name for path
my ($master_file_name) = @ARGV;

chomp $master_file_name;

my $new_file_name = get_new_file_name($master_file_name);

# open files
open my $master_file_handler, '<', $master_file_name
  or die "Can't open file : $!";
open my $new_file_handler, '>>', $new_file_name
  or die "Can't open file : $!";
 
# seek to the beginning of the new file, just to be sure
seek $new_file_handler, 0, 0;

# log processed questions
my $questions_processed = 0;

{
  # split the file by the question separator
  local $/ = $QUESTION_SEPARATOR;
  while(my $question_block = <$master_file_handler>){
    # skip the header
    if ($questions_processed) {
      $question_block = clean($question_block);
      
      my @answers = @{ get_answers($question_block) };

      @answers = @{ shuffle(@answers) };

      $question_block = build($question_block, (join "\n", @answers));
    }

    #write question to the file
    print $new_file_handler $question_block;
    $questions_processed++;
  }
}
 
# close the files
close $master_file_handler or die "Couldn't Close File : $!";
close $new_file_handler or die "Couldn't Close File : $!";

say 'The given exam has been cleaned and shuffled. It is saved here: ' . $new_file_name;

sub get_new_file_name {
  # get the local dir separator
  my $DIR_SEPERATOR = File::Spec->catfile('', '');

  my ($master_file_name) = @_;

  # create timestamp
  my ($sec,$min,$hour,$mday,$mon,$year)=localtime(time);
  my $timestamp = sprintf ( "%04d%02d%02d-%02d%02d%02d", $year+1900,$mon+1,$mday,$hour,$min,$sec);

  # create new file name
  my $new_file_name = $master_file_name;
  substr($new_file_name, rindex($new_file_name, $DIR_SEPERATOR), 1) = $DIR_SEPERATOR . $timestamp . '-';

  return $new_file_name;
}

sub clean {
  my ($question) = @_;

  $question =~ s/\[\s*\w[^]]*\s*\]/\[ \]/xgm;

  return $question;
}

sub get_answers {
  my ($question_block) = @_;

  my @answers = $question_block =~ m/(\N*\[\s*(?:\w|\s)\s*\].*)/gxm;

  return \@answers;
}

sub shuffle {
  my (@answers) = @_;

  # go through each answer swaping it with a random other answer
  for my $answer (@answers) {
    my $rand_swap = rand(scalar @answers);

    ($answer, $answers[$rand_swap]) = ($answers[$rand_swap], $answer);
  }

  return \@answers;
}

sub build {
  my ($question_block, $answers) = @_;

  $question_block =~ s/(?:\N*\[\s*(?:\w|\s)\s*\].*\R)+/$answers\n/mx;

  return $question_block;
}