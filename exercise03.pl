use v5.26;

my $current_dir = '.';
my $file_size = 0;

opendir(DIR, $current_dir) or die $!;

while (my $file = readdir(DIR)) {
  if (!$file_size) {
    $file_size = -s $file;
  }

  if ($file_size < -s $file) {
    say $file;
  }
}

closedir(DIR);